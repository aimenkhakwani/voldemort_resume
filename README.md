## _Voldemort Resume_

#### _A web resume for the The One Who Shall Not Be Named, 8/3/16_

#### By _**Aimen Khakwani and Martin Cartledge**_

## Description

_This is a HTML website created to showcase our HTML and CSS skills._
####  
<img src="screenshot.png" alt="a screenshot of the site">

## Setup/Installation Requirements

*Clone from GitHub*
*Replace with your work and information*

## Support and Contact

_Please contact me through GitHub_

## Technologies Used

_HTML, CSS and Bootstrap_

## License

Copyright (c) 2016 **_Aimen Khakwani and Martin Cartledge_**
